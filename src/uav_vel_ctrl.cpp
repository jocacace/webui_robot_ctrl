#include "ros/ros.h"
#include "boost/thread.hpp"
#include <tf/transform_broadcaster.h>
#include "TooN/TooN.h"

//---Ros message headers
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
//---

//---Mavros specific command
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/CommandCode.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/PositionTarget.h>

#include <mavros_msgs/GlobalPositionTarget.h>
//---


#include "mavros_msgs/WaypointList.h"
#include "geometry_msgs/TwistStamped.h"

using namespace std;

float _vx = 0.0;
float _vy = 0.0;
float _vz = 0.0;
float _wz = 0.0;

#define MAX_VEL 0.5

void twist_cb(geometry_msgs::Twist vel) {


	float wz = vel.angular.z;
	if( wz > 50.0) wz = 50.0;
	if( wz < -50.0) wz = -50.0;
	wz *= 1.0 / 50.0;
	_wz=-wz*MAX_VEL;


	float vz = vel.linear.z;
	if( vz > 50.0) vz = 50.0;
	if( vz < -50.0) vz = -50.0;
	vz *= 1.0 / 50.0;
	_vz=-vz*MAX_VEL;

	float vx = vel.linear.y;
	if( vx > 50.0) vx = 50.0;
	if( vx < -50.0) vx = -50.0;
	vx *= 1.0 / 50.0;
	_vx=vx*MAX_VEL;

	float vy = vel.linear.x;
	if( vy > 50.0) vy = 50.0;
	if( vy < -50.0) vy = -50.0;
	vy *= 1.0 / 50.0;
	_vy=-vy*MAX_VEL;
		

}

int main( int argc, char** argv ) {

	ros::init( argc, argv, "uav_vel_ctrl");
	ros::NodeHandle nh;
	ros::Rate r(10);

	
	ros::Publisher vel_pub = nh.advertise<mavros_msgs::PositionTarget>("/mavros/setpoint_raw/local", 0);
  ros::ServiceClient setmode_client;

	
	/*
	setmode_client = nh.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");
  mavros_msgs::SetMode offb_set_mode;
  offb_set_mode.request.custom_mode = "OFFBOARD";
	if( setmode_client.call(offb_set_mode ) ) {
      ROS_INFO("Offboard enabled");
	}
	*/
  ros::Subscriber sub = nh.subscribe("/webui/cmd_vel", 0, twist_cb);
	mavros_msgs::PositionTarget ptarget;

	while(ros::ok()) {


		 ptarget.type_mask =
      mavros_msgs::PositionTarget::IGNORE_PX |
      mavros_msgs::PositionTarget::IGNORE_PY |
      mavros_msgs::PositionTarget::IGNORE_PZ |
      mavros_msgs::PositionTarget::IGNORE_AFX |
      mavros_msgs::PositionTarget::IGNORE_AFY |
      mavros_msgs::PositionTarget::IGNORE_AFZ |
      mavros_msgs::PositionTarget::FORCE |
      mavros_msgs::PositionTarget::IGNORE_YAW;

    ptarget.coordinate_frame = mavros_msgs::PositionTarget::FRAME_BODY_NED;
    ptarget.velocity.x = _vx;
    ptarget.velocity.y = _vy;
    ptarget.velocity.z = _vz;
    ptarget.yaw_rate = _wz;

    vel_pub.publish( ptarget );

		ros::spinOnce();

		r.sleep();
	}


}
